Uppdaterad team agreement

# Team agreement – open data and open source
## Version
1.3

## Our Mission
The team strengthens Arbetsförmedlingens ability to share and use open data and utilize and contribute to open-source software in digital services and decision-making.

**Process management**: The team manages processes, provides methodological support, hands-on assistance for sharing and using open data and ensures internal and external engagement.

**Open data and open source infrastructure**: The team is responsible for Arbetsförmedlingen part of the national data infrastructure through data.arbetsformedlingen.se.
 
**Upskilling**: The team is tasked with building skills to promote data and API sharing and co-development of open-source software.

**Standardization**: The team participates in standardization efforts to promote interoperability

## Deliverables
Our mission breakes down into the following deliverables:

### Process management
- Methodological support on wiki for providing open data:
- Process descriptions with IPF
- Decision template for providing open data.
- DevSecOps support to those teams working on Gitlab.com/Arbetsformedlingen.
- Metrics on and track the impact of Arbetsformedlingens open data.
- Security measurements (information classification, hash sums and security monitoring)

### Open data infrastructure
- File publishing service on data.arbetsformedlingen.se → Daily harvesting to Sweden’s data portal.
- Support in reporting to Digg (DCAT-AP-SE processor).
- AUB delivery for SUSA. 
- Developing a review tool for REST API profile (Rap.)
- Service now.

### Open Source infrastructure
- An open source collaboration friendly and secure environment on Gitlab.com/Arbetsformedlingen, making it possible for external parties to collaborate with us on our open source code. 
- Automated security checks throughout the software lifecycle and support development teams with security expertise.
- Vulnerabilities monitoring, providing risk updates, managing urgent responses, including software updates or shutdowns in critical situations.

### Upskilling
- Promotion initiatives and capability development through Sänkta trösklar.
- Standardization
- Development of the EMIL standard (Educational data) and participation in the EMIL committee.
- Participation in Ena’s API building block.
- Participation in the community for Standard for public code.

### Meetings and check-ins
- Weekly meetings within the open data team.
- Participation in DevSecOps weekly meetings.
- Participation in CC2.
- Sänkta trösklar meetings every third week.
- MSV KLL Datainfrastruktur meetings
- EMIL standard meetings.
- Standard for public code meeting every third week
- API BB - meetings
- Meetings with external stakeholders as needed.
 
# APPENDIX: Goals for 2025 (Epics?)

- Develop a design pattern for sharing data as files and APIs.++++++++
- Ensure increased open data offerings from the agency.+++++++
- Comprehensive approach to Metadata management (Template).+++
- Upskilling Arbetsförmedlingen work with open source software, by creating a checklist or some other methodological support. +++
- Unified GitLab management according to the Standard for Public Code (documentation and issue tracking). 
- Disseminate information on the new decision template and methodological support.

## Members
Who has agreed to the Team agreement: 
@perui, @Sodjs, @maria.dalhage, @lofms, @perweij, @jave, @halge.

